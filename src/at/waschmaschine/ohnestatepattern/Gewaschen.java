package at.waschmaschine.ohnestatepattern;

public class Gewaschen implements State {
    @Override
    public void euroEinwerfen(StateWaschmaschine context) {
        System.out.println("Waesche wurde bereits gewaschen, kein Euro mehr notwendig");
    }

    @Override
    public void euroAuswerfen(StateWaschmaschine context) {
        System.out.println("Waesche wurde bereits gewaschen");
    }

    @Override
    public void waschen(StateWaschmaschine context) {
        System.out.println("Waesche wurde bereits gewaschen");
    }

    @Override
    public void waescheEntfernen(StateWaschmaschine context) {
        System.out.println("Ihre Waesche ist nun sauber");
        if(context.anzahlMuenzen >= context.kapazitaetMunzbehaelter){
            context.setState(new MuenzbehaelterVoll());
        }
        else{
            context.setState(new KeinEuro());
        }
    }

    @Override
    public void muenzbehaelterLeeren(StateWaschmaschine context) {
        System.out.println("Muenzbehaelter nicht voll");
    }
}
