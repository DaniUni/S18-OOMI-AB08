package at.waschmaschine.ohnestatepattern;

public class MuenzbehaelterVoll implements State {
    @Override
    public void euroEinwerfen(StateWaschmaschine context) {
        System.out.println("Muenzbehaelter voll, wenden Sie sich an das Servicepersonal");
    }

    @Override
    public void euroAuswerfen(StateWaschmaschine context) {
        System.out.println("Es wurde vorher kein Euro eingeworfen");
    }

    @Override
    public void waschen(StateWaschmaschine context) {
        System.out.println("Waschen nicht moeglich");
    }

    @Override
    public void waescheEntfernen(StateWaschmaschine context) {
        System.out.println("Es wurde keine Waesche gewaschen");
    }

    @Override
    public void muenzbehaelterLeeren(StateWaschmaschine context) {
        System.out.println("Muenzbehaelter geleert");
        context.anzahlMuenzen = 0;
    }
}
