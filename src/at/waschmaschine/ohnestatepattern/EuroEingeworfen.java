package at.waschmaschine.ohnestatepattern;

public class EuroEingeworfen implements State{
    @Override
    public void euroEinwerfen(StateWaschmaschine context) {
        System.out.println("Es wurde bereits ein Euro eingeworfen");
    }

    @Override
    public void euroAuswerfen(StateWaschmaschine context) {
        System.out.println("Euro wird ausgeworfen");
        context.anzahlMuenzen--;
        context.setState(new KeinEuro());
    }

    @Override
    public void waschen(StateWaschmaschine context) {
        System.out.println("Waesche wird gewaschen");
        context.setState(new Gewaschen());
    }

    @Override
    public void waescheEntfernen(StateWaschmaschine context) {
        System.out.println("Es wurde keine Waesche gewaschen");
    }

    @Override
    public void muenzbehaelterLeeren(StateWaschmaschine context) {
        System.out.println("Muenzbehaelter nicht voll");
    }
}
