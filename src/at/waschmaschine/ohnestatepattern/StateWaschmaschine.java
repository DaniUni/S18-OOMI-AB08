package at.waschmaschine.ohnestatepattern;

public class StateWaschmaschine {
    private State currentState;

    public int kapazitaetMunzbehaelter;
    public int anzahlMuenzen;

    public StateWaschmaschine(int kapazitaetMunzbehaelter){
        this.kapazitaetMunzbehaelter = kapazitaetMunzbehaelter;
        currentState = new KeinEuro();
    }

    public void setState(State s){
        currentState = s;
    }


    public void euroEinwerfen() { currentState.euroEinwerfen(this); }

    public void waschen() { currentState.waschen(this); }

    public void euroAuswerfen() { currentState.euroAuswerfen(this); }

    public void waescheEntfernen() { currentState.waescheEntfernen(this); }
}
