package at.waschmaschine.ohnestatepattern;

public class KeinEuro implements State {
    @Override
    public void euroEinwerfen(StateWaschmaschine context) {
        context.anzahlMuenzen++;
        context.setState(new EuroEingeworfen());
    }

    @Override
    public void euroAuswerfen(StateWaschmaschine context) {
        System.out.println("Es wurde vorher kein Euro eingeworfen");
    }

    @Override
    public void waschen(StateWaschmaschine context) {
        System.out.println("Kein Euro eingeworfen, waschen nicht moeglich");
    }

    @Override
    public void waescheEntfernen(StateWaschmaschine context) {
        System.out.println("Es wurde keine Waesche gewaschen");
    }

    @Override
    public void muenzbehaelterLeeren(StateWaschmaschine context) {
        System.out.println("Muenzbehaelter nicht voll");
    }
}
