package at.waschmaschine.ohnestatepattern;

public interface State {
    public void euroEinwerfen(StateWaschmaschine context);
    public void euroAuswerfen(StateWaschmaschine context);
    public void waschen(StateWaschmaschine context);
    public void waescheEntfernen(StateWaschmaschine context);
    public void muenzbehaelterLeeren(StateWaschmaschine context);
}
